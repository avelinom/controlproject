package br.com.lp3.controlproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.controlproject.dto.StatusDTO;
import br.com.lp3.controlproject.service.StatusService;

@RestController
@RequestMapping(value = "/status")
public class StatusController {

	@Autowired
	private StatusService statusService;
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<StatusDTO> saveStatus(@RequestBody StatusDTO statusDTO) {
		statusDTO = statusService.saveStatus(statusDTO);
		if (statusDTO != null) {
			return new ResponseEntity<StatusDTO>(statusDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<StatusDTO>(statusDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
