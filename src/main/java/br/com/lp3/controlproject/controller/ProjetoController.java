package br.com.lp3.controlproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.controlproject.dto.ProjetoDTO;
import br.com.lp3.controlproject.service.ProjetoService;

@RestController
@RequestMapping(value = "/projeto")
public class ProjetoController {

	@Autowired
	private ProjetoService projetoService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProjetoDTO> findProjetoById(@PathVariable Long id) {
		ProjetoDTO projetoDTO = projetoService.findById(id);
		if (projetoDTO != null) {
			return new ResponseEntity<ProjetoDTO>(projetoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProjetoDTO>(projetoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<ProjetoDTO> saveProjeto(@RequestBody ProjetoDTO projetoDTO) {
		projetoDTO = projetoService.saveProjeto(projetoDTO);
		if (projetoDTO != null) {
			return new ResponseEntity<ProjetoDTO>(projetoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProjetoDTO>(projetoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<ProjetoDTO> updateProjeto(@PathVariable Long id, @RequestBody ProjetoDTO projetoDTO) {
		projetoDTO = projetoService.saveProjeto(projetoDTO);
		if (projetoDTO != null) {
			return new ResponseEntity<ProjetoDTO>(projetoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProjetoDTO>(projetoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> removeProjeto(@PathVariable Long id) {
		projetoService.removeProjeto(id);
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public ResponseEntity<List<ProjetoDTO>> findAll() {
		List<ProjetoDTO> list = projetoService.findAll();
		return new ResponseEntity<List<ProjetoDTO>>(list, HttpStatus.OK);
	}
}
