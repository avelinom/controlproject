package br.com.lp3.controlproject.dto;

import java.io.Serializable;

public class ProjetoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String nome;
	private String descricao;
	private String dataInicio;
	private String dataLimite;
	private StatusDTO status;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataLimite() {
		return dataLimite;
	}
	public void setDataLimite(String dataLimite) {
		this.dataLimite = dataLimite;
	}
	public StatusDTO getStatus() {
		return status;
	}
	public void setStatus(StatusDTO status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ProjetoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
