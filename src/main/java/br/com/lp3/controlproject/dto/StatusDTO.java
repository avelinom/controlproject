package br.com.lp3.controlproject.dto;

import java.io.Serializable;

public class StatusDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String status;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public StatusDTO(Long id, String status) {
		super();
		this.id = id;
		this.status = status;
	}
	public StatusDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
