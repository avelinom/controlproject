package br.com.lp3.controlproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.StatusDTO;
import br.com.lp3.controlproject.model.Status;
import br.com.lp3.controlproject.repository.StatusRepository;

@Service
public class StatusService {

	@Autowired
	private StatusRepository statusRepository;

	public StatusDTO saveStatus(StatusDTO statusDTO) {
		Status status = new Status(statusDTO.getId(), statusDTO.getStatus());
		status = statusRepository.save(status);
		statusDTO.setId(status.getId());
		return statusDTO;
	}

}
